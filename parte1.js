	/*   declaracion de variables globales   */

	var x = [];
	var y = [];
	var seleccionadas = [];
	var correctas = [];
	var incorrectas = [];
	var a;
	var posicionesc = [];
	var posicionesi = [];
	var acertadas = [3, 1, 4, 3, 3, 2, 1, 3, 4, 2];
	var h = 0;
	x = ["1.	¿Qué es Windows 7?:", "2.  ¿Qué afirmación de las siguientes es cierta?:",
	    "3.	¿Qué afirmación de las siguientes es cierta?:", "4.	¿Qué afirmación de las siguientes es cierta?:",
	    "5.	¿Cuál es la frase correcta sobre barras de herramientas en ventanas de aplicación?:",
	    "6.	¿Qué afirmación de las siguientes es cierta?:", "7.	¿Qué afirmación de las siguientes es cierta?:",
	    "8.	¿Qué afirmación de las siguientes es cierta?:", "9.	¿Cómo se ejecuta un programa con el botón Inicio?:",
	    "10.	¿Qué versión de Windows 7 no existe?:"
	]

	y = [
	    ["Un examinador web", "Un motor de búsqueda", "Un sistema operativo", "Un procesador de textos"],
	    ["La barra de tareas puede moverse a la parte superior o inferior del escritorio o a cualquiera de sus lados",
	        "La barra de tareas puede moverse al centro del Escritorio", "La barra de tareas no puede moverse a ninguna otra posición",
	        "La barra de tareas sólo puede moverse cuando no se están ejecutando aplicaciones"
	    ],
	    ["El botón Inicio puede separarse de la barra de tareas", "El botón Inicio sólo debería utilizarse en emergencias",
	        "El botón Inicio le permite ejecutar aplicaciones ", "El botón Inicio puede eliminarse del Escritorio "
	    ],
	    ["El botón Minimizar se usa para eliminar temporalmente una ventana del Escritorio",
	        "El botón Minimizar se usa para cerrar una ventana", "El botón Minimizar se usa para reducir el Escritorio",
	        "El botón Minimizar se usa para reducir la barra de tareas"
	    ],
	    ["Las barras de herramientas siempre aparecen en la parte inferior de la ventana",
	        "Las barras de herramientas sólo proporcionan acceso a comandos no disponibles en los menús",
	        "Las barras de herramientas proporcionan acceso rápido a comandos generalmente disponibles en los menús",
	        "Las barras de herramientas proporcionan acceso a los mismos comandos que están disponibles en los menús"
	    ],
	    ["Las barras de desplazamiento pueden utilizarse para copiar información entre aplicaciones",
	        "Una barra de desplazamiento vertical sólo será visible si la información es demasiado extensa para caber en la ventana",
	        "Las barras de desplazamiento pueden desplazar la información de forma diagonal, horizontal y vertical",
	        "Las barras de desplazamiento permiten minimizar, maximizar o cerrar una ventana abierta"
	    ],
	    ["Las carpetas pueden almacenar otras carpetas", "Una carpeta sólo puede contener documentos del mismo tipo",
	        "Cada carpeta debe guardarse en una unidad distinta", "Las carpetas se eliminan automáticamente al quedar vacías"
	    ],
	    ["Puede acceder a la Ayuda de Windows haciendo clic derecho en el Escritorio",
	        "Puede acceder a la Ayuda de Windows desde la barra de menús de cualquier aplicación",
	        "Puede acceder a la Ayuda de Windows desde el menú Inicio ",
	        "Puede acceder a la Ayuda de Windows haciendo clic derecho en la barra de tareas"
	    ],
	    ["Haciendo clic derecho en el botón Inicio y seleccionando un programa de la lista que aparece",
	        "Haciendo clic derecho sobre el botón Inicio y seleccionando Ayuda",
	        "Haciendo doble clic sobre el botón Inicio y seleccionando Ayuda",
	        "Haciendo clic en el botón Inicio, seleccionando Programas, una carpeta de programa y el programa que desee"
	    ],
	    ["Windows 7 32 bits", "Windows 7 48 bits", "Windows 7 64 bits", "Windows 7 Starter"]
	]

	/*   funcion que crea etiquetas y ejecuta el test   */

	function iniciar() {
	    for (var i = 0; i < x.length; i++) {

	        document.querySelector(".wrapper").innerHTML += "<fieldset></fieldset>";
	        document.querySelectorAll("fieldset")[i].innerHTML = "<legend></legend>";
	        b = document.querySelectorAll("legend")[i].innerHTML = x[i];
	        for (h = 0; h < 4; h++) {
	            document.querySelectorAll("fieldset")[i].innerHTML += "<input type='radio' name='test" + (i) + "' value='" + (h + 1) + "''>";
	            document.querySelectorAll("fieldset")[i].innerHTML += y[i][h] + "<br>";
	        }
	    }
	    document.querySelector("button").addEventListener("click", examinar);

	}

	/*   funcion que coteja preguntas y respuestas   */

	function examinar() {

	    for (var i = 0; i < 10; i++) {
	        a = document.getElementsByName("test" + i);
	        for (var h = 0; h < 4; h++) {
	            if (a[h] = a[h].checked) { seleccionadas.push(parseInt(a[h].value)) }

	        }
	    }
	    if (seleccionadas.length < 10) { alert('Faltan preguntas por contestar');
	        selecionadas = []; return; } else {

	        for (var i = 0; i < acertadas.length; i++) {
	            if (acertadas[i] == seleccionadas[i]) {
	                correctas.push(seleccionadas[i]);
	                posicionesc.push(i);
	            } else {
	                incorrectas.push(seleccionadas[i]);
	                posicionesi.push(i);
	            }
	        }
	        resultado();
	    }
	}

	/*   funcion que muestra resultados   */

	function resultado() {

	    document.querySelector(".modal").style.zIndex = "20";
	    document.querySelector(".modal").style.opacity = 1;

	    document.querySelector(".modal2").innerHTML += "<h3>--Respuestas correctas </h3>" + (correctas.length) + "<br>";
	    document.querySelector(".modal2").innerHTML += "<h3>--Respuestas incorrectas </h3>" + (incorrectas.length) + "<br>";
	    document.querySelector(".modal2").innerHTML += "<h3>--Preguntas acertadas:</h3>" + "<br>";
	    for (var i = 0; i < correctas.length; i++) {

	        document.querySelector(".modal2").innerHTML += x[posicionesc[i]] + "<br>";
	        document.querySelector(".modal2").innerHTML += y[posicionesc[i]][(correctas[i]) - 1] + "<br>";
	    }
	    document.querySelector(".modal2").innerHTML += "<h3>--Respuestas falladas:</h3>" + "<br>";
	    for (var i = 0; i < incorrectas.length; i++) {
	        document.querySelector(".modal2").innerHTML += x[posicionesi[i]] + "<br>";
	        document.querySelector(".modal2").innerHTML += y[posicionesi[i]][(incorrectas[i]) - 1] + "<br>";
	    }
	    document.querySelector(".modal2").innerHTML += "<h1>Puntuacion " + correctas.length + "/" + acertadas.length + "</h1>";

	    document.querySelector(".boton2").addEventListener("click", previo);
	}

	function previo() {


	    document.location.href = "file:///C:/Users/david/Documents/Curso%20desarrollo/m1/u2/practicas/practica5/parte1.html";

	}